package org.abbtech.lesson2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.ArgumentMatchers.anyInt;

@ExtendWith(MockitoExtension.class)
public class ApplicationServiceUnitTest {
    @Mock
    private CalculatorService calculatorService;
    @InjectMocks
    private ApplicationService applicationService;

    @Test
    void multiply_success() {
        Mockito.when(calculatorService.multiply(anyInt(), anyInt())).thenReturn(10);
        int actual = applicationService.multiply(3, 4);
        Assertions.assertEquals(12, actual);
    }
    @Test
    void subtract_success() {

    }
}

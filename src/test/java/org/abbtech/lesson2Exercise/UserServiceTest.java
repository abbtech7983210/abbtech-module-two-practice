package org.abbtech.lesson2Exercise;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
     @Mock
     private UserRepository userRepository;
     @InjectMocks
     private UserService userService;

     @Test
     void testIsUserActive_UserFound() {
         String username = "testUser";
         User user = new User(username, true);
         when(userRepository.findByUsername(username)).thenReturn(user);
         assertTrue(userService.isUserActive(username));
     }

     @Test
    void UserNotFound() {
        String username = "testUnfoundedUser";
        when(userRepository.findByUsername(username)).thenReturn(null);
        assertTrue(userService.isUserActive(username));
    }

    @Test
    void testDeleteUser_UserFound() {
         Integer userId = 1;
        when(userRepository.findByUserId(userId)).thenReturn(new User("testUser", true));
        assertDoesNotThrow(() -> userService.deleteUser(userId));
    }

    @Test
    void testDeleteUser_UserNotFound() {
        Integer userId = 1;
        when(userRepository.findByUserId(userId)).thenReturn(null);
        assertThrows(Exception.class, () -> userService.deleteUser(userId));
    }

    @Test
    void testGetUser_UserFound() throws Exception {
        Integer userId = 1;
        User user = new User("testUser", true);
        when(userRepository.findByUserId(userId)).thenReturn(user);

        assertEquals(user, userService.getUser(userId));
    }

    @Test
    void testGetUser_UserNotFound() {
        Integer userId = 1;
        when(userRepository.findByUserId(userId)).thenReturn(null);

        assertThrows(Exception.class, () -> userService.getUser(userId));
    }

}

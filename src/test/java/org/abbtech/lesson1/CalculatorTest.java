package org.abbtech.lesson1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CalculatorTest {

    Calculator calculator;

    @BeforeEach
    void init() {
        calculator = new Calculator();
    }

    @Test
    void multiplyTest() {
        int actualResult = calculator.multiply(10, 5);
        Assertions.assertEquals(actualResult, 50);
    }

    @Test
    void addTest() {
        int actualResult = calculator.add(10, 5);
        Assertions.assertEquals(actualResult, 15);
    }

    @Test
    void subtractTest() {
        int actualResult = calculator.subtract(10, 5);
        Assertions.assertEquals(actualResult, 5);
    }

    @Test
    void divideTest() {
        double actualResult = calculator.divide(10, 5);
        Assertions.assertEquals(actualResult, 2);
    }

    @Test
    void divideByZeroTest() {
        Assertions.assertThrows(ArithmeticException.class, () -> calculator.divide(10, 0));
    }

    @Test
    void largeNumbersTest() {
        int actualResult = calculator.multiply(Integer.MAX_VALUE, 2);
        Assertions.assertEquals(Integer.MAX_VALUE * 2, actualResult);
    }

    @Test
    void negativeNumbersTest() {
        int actualResult = calculator.add(-10, -5);
        Assertions.assertEquals(-15, actualResult);
    }
}

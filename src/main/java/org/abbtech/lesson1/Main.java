package org.abbtech.lesson1;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        System.out.println("Result: " + calculator.multiply(3, 4));
        System.out.println("Result: " + calculator.add(3, 4));
        System.out.println("Result: " + calculator.subtract(20, 4));
        System.out.println("Result: " + calculator.divide(24, 6));
    }
}

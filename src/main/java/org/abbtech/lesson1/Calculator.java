package org.abbtech.lesson1;

public class Calculator {

    public  int a;
    public int b;

    public int multiply(int a, int b) {
        return a * b;
    }

    public int add(int a, int b) {
        return a + b;
    }

    public int subtract(int a, int b) {
        return a - b;
    }

    public double divide(int a, int b) {
        return a / b;
    }
}

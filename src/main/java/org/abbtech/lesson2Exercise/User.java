package org.abbtech.lesson2Exercise;

public class User {
    private String username;
    private Integer id;
    private boolean active;

    public User(String username, boolean active) {
        this.username = username;
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }
}

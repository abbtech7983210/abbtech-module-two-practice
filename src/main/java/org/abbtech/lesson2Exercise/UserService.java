package org.abbtech.lesson2Exercise;

public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean isUserActive(String username) {
        User user = userRepository.findByUsername(username);
        return user != null && user.isActive();
    }

    public void deleteUser(Integer userId) throws Exception {
        User user = userRepository.findByUserId(userId);
        if (user == null) {
            throw new Exception("User not found");
        }
    }

    public User getUser(Integer userId) throws Exception {
        User user = userRepository.findByUserId(userId);
        if (user == null) {
            throw new Exception("User not found");
        }
        return user;
    }
}

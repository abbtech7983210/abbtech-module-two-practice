package org.abbtech.lesson2Exercise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer>  {
    User findByUsername(String username);
    User findByUserId(Integer uerId);
}
